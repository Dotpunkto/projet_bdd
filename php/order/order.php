<?php

    class order {
        
        function parseOrder($bdd, $orderData) {
            $bdd->beginTransaction();
            foreach ($orderData as $key => $order) {
                $sql = "INSERT INTO ordered(customerID, productID, orderDate, colorID, quantity, amount) 
                VALUES (:customerID, :productID, :orderDate, :colorID, :quantity, :amount)";
                $res = $bdd->prepare($sql);
                $indexColor = $this->getColorId($bdd, $order['E']);
                $indexCustomer = substr($order['B'], 9);
                $indexProduct = substr($order['C'], 8);
                $quantity = rand(1,50);
                $amount = $this->getPrice($bdd, $indexProduct) * $quantity;
                $res->bindParam(':customerID', $indexCustomer);
                $res->bindParam(':productID', $indexProduct);
                $res->bindParam(':orderDate', $order['D']);
                $res->bindParam(':colorID', $indexColor);
                $res->bindParam(':quantity', $quantity);
                $res->bindParam(':amount', $amount);
                $res->execute(); 
            }
            $bdd->commit();
        }

        private function getPrice($bdd, $productID) {
            $sql = "SELECT price FROM product WHERE productID LIKE '%$productID%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $price = $array["price"];
            return $price;
        }

        private function getColorId($bdd, $color) {
            $sql = "SELECT colorID FROM color WHERE color LIKE '%$color%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $id = $array["colorID"];
            return $id;
        }

        function getOrder($bdd, $start, $limite) {
            $sql = "SELECT orderID, customer.lastName, customer.firstName, product.productName, orderDate, color.color, quantity, amount FROM ordered LEFT JOIN color ON ordered.colorID = color.colorID LEFT JOIN customer ON ordered.customerID = customer.customerID LEFT JOIN product ON ordered.productID = product.productID LIMIT $start,$limite";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;
        }

        function displayOrder($arrayOrder) {
            ?>
                <div class="card-body">
                    <div class="row d-flex justify-content-center">
            <?php
            foreach ($arrayOrder as $key => $value) {
            ?>
                <div class="card border-primary mb-3 ml-3 mr-3 mt-3" style="max-width: 18rem; min-height: 18rem;">
                    <div class="card-header"><?= $value["orderID"]?> : <?=$value["lastName"]?> <?=$value["firstName"]?></div>
                    <div class="card-body text-primary">
                        <h5 class="card-title"><?= $value["productName"]?></h5>
                        <p class="card-text">Date: <?= $value["orderDate"]?></p>
                        <p class="card-text">Couleur: <?= $value["color"]?></p>
                        <p class="card-text">Quantité: <?= $value["quantity"]?></p>
                        <p class="card-text">Total: <?= $value["amount"]?> $</p>
                    </div>
                </div>
            <?php
            }
            ?>
                    </div>
                </div>
            <?php
        }

        function getOrderCount($bdd) {
            $itemsNumber = $bdd->query("SELECT count(orderID) FROM ordered");
            $array = $itemsNumber->fetch();
            return intval($array["count(orderID)"]);
        }
    }

?>
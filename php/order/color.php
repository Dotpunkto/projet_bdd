<?php

    class color {

        function parseColor($bdd, $arrayColor) {
            $bdd->beginTransaction();
            foreach ($arrayColor as $key => $value) {
                $sql = "INSERT INTO color(color) VALUES (:color)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':color', $value);
                $res->execute();
            }
            $bdd->commit();
        }

        function notDuplicates($orderData) {
            $arrayColor = array();
            foreach ($orderData as $key => $order) {
                array_push($arrayColor, $order['E']);
            }
            return array_unique($arrayColor);
        }

    }

?>
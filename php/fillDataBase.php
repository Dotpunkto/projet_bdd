<?php
    
    require 'php/get_data.php';
    require 'php/product/brand.php';
    require 'php/product/product.php';
    require 'php/customer/country.php';
    require 'php/customer/customer.php';
    require 'php/order/color.php';
    require 'php/order/order.php';
    
    try {
        $bdd = new PDO('sqlite:bdd/base.db');
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }

    $data = new getData();
    $productData = $data->getProductData();
    $customerData = $data->getCustomerData();
    $orderData = $data->getOrderData();

    $customer = new customer();
    $customerCount = $customer->getCustomerCount($bdd);
    $order = new order();
    $orderCount = $order->getOrderCount($bdd);
    $product = new product();
    $productCount = $product->getProductCount($bdd);
    
    if($productCount < count($productData)) {
        $brand = new brand();
        $arrayBrand = $brand->noDuplcates($productData);
        $brand->parseBrand($bdd, $arrayBrand);
        $product->parseProduct($bdd, $productData);
    }

    if($customerCount < count($customerData)) {
        $country = new country();
        $arrayCountry = $country->notDuplicates($customerData);
        $country->parseCountry($bdd, $arrayCountry);
        $customer->parseCustomer($bdd, $customerData);
    }

    if($orderCount < count($orderData)) {
        $color = new color();
        $arrayColor = $color->notDuplicates($orderData);
        $color->parseColor($bdd, $arrayColor);
        $order->parseOrder($bdd, $orderData);
    }

?>
<?php

    require 'php/product/brand.php';
    require 'php/product/product.php';
    require 'php/display.php';

    try {
        $bdd = new PDO('sqlite:bdd/base.db');
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }

    $limite = 4;
    $display = new display();
    $pageMax = $display->getPageMax($bdd, "productID", "product", $limite);
    $start = $display->getStart($pageMax, $limite);
    $currentPage = $display->getCurrentPage();
    $display->paging($currentPage, $pageMax, "index2");

    $product = new product();
    $arrayProduct = $product->getProduct($bdd, $start, $limite);
    $product->displayProduct($arrayProduct);
?>
<?php

    class display {

        function getStart($pageMax, $limite) {            
            if(isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] > 0 && $_GET['page'] <= $pageMax) {
                $_GET['page'] = intval($_GET['page']);
                $currentPage = $_GET['page'];
            } else {
                $currentPage = 1;
            }
            return ($currentPage-1)*$limite;
        }

        function getPageMax($bdd, $column, $table, $limite) {
            $itemsNumber = $bdd->query("SELECT count($column) FROM $table");
            $array = $itemsNumber->fetch();
            $numberOrder = intval($array["count($column)"]);
            return ceil($numberOrder/$limite);
        }

        function getCurrentPage() {
            if(empty($_GET['page'])) {
                return 1;
            }
            return intval($_GET['page']);
        }

        function paging($currentPage, $pageMax, $page) {
            ?>
                <nav aria-label="Page navigation" style="margin-top: 5px;">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                    <?php
                    if ($currentPage-1 < 1) {
                            ?>
                                <a class="page-link disabled" href="<?=$page?>.php?page=<?=$currentPage?>">Previous</a>
                            <?php
                        } else {
                            ?>
                                <a class="page-link" href="<?=$page?>.php?page=<?=$currentPage-1?>">Previous</a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                        for($i=1; $i <= $pageMax; $i++) {
                            if ($i == $currentPage) {
                                ?>
                                <li class="page-item disabled"><a class="page-link" href="<?=$page?>.php?page=<?=$i?>"><?=$i?></a></li>
                                <?php
                            } else {
                                ?>
                                <li class="page-item"><a class="page-link" href="<?=$page?>.php?page=<?=$i?>"><?=$i?></a></li>
                                <?php
                            }
                        }
                    ?>
                    <?php
                        if ($currentPage+1 > $pageMax) {
                            ?>
                                <a class="page-link disabled" href="<?=$page?>.php?page=<?=$currentPage?>">Next</a>
                            <?php
                        } else {
                            ?>
                                <a class="page-link" href="<?=$page?>.php?page=<?=$currentPage+1?>">Next</a>
                            <?php
                        }
                    ?>
                    </li>
                </ul>
                </nav>
            <?php
        }
    }

?>
<?php

    class country {
        function parseCountry($bdd, $arrayCountry) {
            $bdd->beginTransaction();
            foreach ($arrayCountry as $key => $value) {
                $sql = "INSERT INTO country(country) VALUES (:country)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':country', $value);
                $res->execute();
            }
            $bdd->commit();
        }

        function notDuplicates($customerData) {
            $arrayCountry = array();
            foreach ($customerData as $key => $customer) {
                array_push($arrayCountry, $customer['H']);
            }
            return array_unique($arrayCountry);
        }

    }
?>
<?php
    
    class customer {
        
        function parseCustomer($bdd, $customerData) {
            $bdd->beginTransaction();
            foreach ($customerData as $key => $customer) {
                $sql = "INSERT INTO customer(lastName, firstName, mail, phone, countryID, city, customerAddress) VALUES (:lastName, :firstName, :mail, :phone, :countryID, :city, :customerAddress)";
                $res = $bdd->prepare($sql);
                $id = $this->getCountryId($bdd, $customer['H']);
                $res->bindParam(':lastName', $customer['B']);
                $res->bindParam(':firstName', $customer['C']);
                $res->bindParam(':mail', $customer['D']);
                $res->bindParam(':phone', $customer['E']);
                $res->bindParam(':city', $customer['F']);
                $res->bindParam(':customerAddress', $customer['G']);
                $res->bindParam(':countryID', $id);
                $res->execute();
            }
            $bdd->commit();
        }

        private function getCountryId($bdd, $country) {
            $sql = "SELECT countryID FROM country WHERE country LIKE '%$country%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $id = $array["countryID"];
            return $id;
        }

        function getCustomer($bdd, $start, $limite) {
            $sql = "SELECT customerID, lastName, firstName, mail, phone, country.country, city, customerAddress FROM customer LEFT JOIN country ON customer.countryID = country.countryID LIMIT $start,$limite";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;
        }

        function displayCustomer($customer) {
            ?>
                <div class="card-body">
                    <div class="row d-flex justify-content-center">
            <?php
            foreach ($customer as $key => $value) {
            ?>
                <div class="card border-primary mb-3 ml-3 mr-3 mt-3" style="max-width: 18rem;">
                    <div class="card-header"><?= $value["customerID"]?> : <?=$value["lastName"]?> <?=$value["firstName"]?></div>
                    <div class="card-body text-primary">
                        <h5 class="card-title"><?= $value["country"]?> <?= $value["city"]?> <?= $value["customerAddress"]?></h5>
                        <p class="card-text">Mail: <?= $value["mail"]?></p>
                        <p class="card-text">Phone: <?= $value["phone"]?></p>
                    </div>
                </div>
            <?php
            }
            ?>
                    </div>
                </div>
            <?php
        }

        function getCustomerCount($bdd) {
            $itemsNumber = $bdd->query("SELECT count(customerID) FROM customer");
            $array = $itemsNumber->fetch();
            return intval($array["count(customerID)"]);
        }
    }
?>
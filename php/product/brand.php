<?php
    
    class brand {

        function parseBrand($bdd, $arrayBrand) {
            $bdd->beginTransaction();
            foreach ($arrayBrand as $key => $value) {
                $sql = "INSERT INTO brand(brands) VALUES (:brands)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':brands', $value);
                $res->execute();
            }
            $bdd->commit();
        }

        function noDuplcates($productData)  {
            $arrayBrand = array();
            foreach ($productData as $key => $product) {
                array_push($arrayBrand, $product['C']);
            }
            return array_unique($arrayBrand);
        }
        
    }
    

?>
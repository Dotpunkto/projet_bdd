<?php

    class product {
        
        function parseProduct($bdd, $productData) {
            $bdd->beginTransaction();
            foreach ($productData as $key => $product) { 
                $sql = "INSERT INTO product(productName, brandID, productDescription, productUrl, tags, price) VALUES (:productName, :brandID, :productDescription, :productUrl, :tags, :price)";
                $res = $bdd->prepare($sql);
                $brandIndex = $this->getBrandId($bdd, $product['C']);
                $res->bindParam(':productName', $product['B']);
                $res->bindParam(':brandID', $brandIndex);
                $res->bindParam(':productDescription', $product['D']);
                $res->bindParam(':productUrl', $product['E']);
                $res->bindParam(':tags', $product['F']);
                $res->bindParam(':price', $product['G']);
                $res->execute();
            }
            $bdd->commit();
        }

        private function getBrandId($bdd, $brand) {
            $sql = "SELECT brandID FROM brand WHERE brands LIKE '%$brand%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $id = $array["brandID"];
            return $id;
        }

        function getProduct($bdd, $start, $limite) {
            $sql = "SELECT productID, productName, brand.brands, productDescription, productUrl, tags, price FROM product LEFT JOIN brand ON product.productID = brand.brandID LIMIT $start,$limite";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;
        }

        function displayProduct($product) {
            ?>
                <div class="card-body">
                    <div class="row d-flex justify-content-center">
            <?php
            foreach ($product as $key => $value) {
            ?>
                <div class="card border-primary mb-3 ml-3 mr-3 mt-3" style="max-width: 18rem;">
                    <div class="card-header"><?= $value["productID"]?> : <?=$value["productName"]?></div>
                    <div class="card-body text-primary">
                        <h5 class="card-title"><?= $value["brands"]?></h5>
                        <p class="card-text">Description: <?= $value["productDescription"]?></p>
                        <p class="card-text">Lien: <?= $value["productUrl"]?></p>
                        <p class="card-text">Tags: <?= $value["tags"]?></p>
                        <p class="card-text">Prix: <?= $value["price"]?> $</p>
                    </div>
                </div>
            <?php
            }
            ?>
                    </div>
                </div>
            <?php
        }

        function getProductCount($bdd) {
            $itemsNumber = $bdd->query("SELECT count(productID) FROM product");
            $array = $itemsNumber->fetch();
            return intval($array["count(productID)"]);
        }
    }
    

?>
<?php

    require 'vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

    class getData {

        private function getReader() {
            $reader = new Xlsx();
            return $reader->load("bdd/database4.xlsx");
        }
    
        function getCustomerData() {
            $spreadsheet = $this->getReader();
            return $sheetCustomerData = $spreadsheet->getSheet(0)->toArray(NULL, TRUE, TRUE, TRUE);
        }
    
        function getOrderData() {
            $spreadsheet = $this->getReader();        
            return $sheetOrderData = $spreadsheet->getSheet(1)->toArray(NULL, TRUE, TRUE, TRUE);
        }
    
        function getProductData() {
            $spreadsheet = $this->getReader();        
            return $sheetProductData = $spreadsheet->getSheet(2)->toArray(NULL, TRUE, TRUE, TRUE);
        }
    }

?>
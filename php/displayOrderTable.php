<?php

    require 'php/order/color.php';
    require 'php/order/order.php';
    require 'php/display.php';
    

    try {
        $bdd = new PDO('sqlite:bdd/base.db');
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
    
    $limite = 1000;
    $display = new display();
    $pageMax = $display->getPageMax($bdd, "orderID", "ordered", $limite);
    $start = $display->getStart($pageMax, $limite);
    $currentPage = $display->getCurrentPage();
    $display->paging($currentPage, $pageMax, "index3");

    $order = new order();
    $arrayOrder = $order->getOrder($bdd, $start, $limite);
    $order->displayOrder($arrayOrder);

?>
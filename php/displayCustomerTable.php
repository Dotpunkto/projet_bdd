<?php

    require 'php/display.php';

    try {
        $bdd = new PDO('sqlite:bdd/base.db');
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }

    $limite = 5;
    $display = new display();
    $pageMax = $display->getPageMax($bdd, "customerID", "customer", $limite);
    $start = $display->getStart($pageMax, $limite);
    $currentPage = $display->getCurrentPage();
    $display->paging($currentPage, $pageMax, "index");

    $customer = new customer();
    $arrayCustomer = $customer->getCustomer($bdd, $start, $limite);
    $customer->displayCustomer($arrayCustomer);
    
?>